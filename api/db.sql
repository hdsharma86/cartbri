--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.13
-- Dumped by pg_dump version 9.5.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO postgres;

--
-- Name: address_books; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.address_books (
    id integer NOT NULL,
    customers_id integer,
    gender character varying(255),
    company character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    street_address character varying(255),
    suburb character varying(255),
    postcode character varying(255),
    city character varying(255),
    state character varying(255),
    country_id integer,
    zone_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.address_books OWNER TO postgres;

--
-- Name: address_books_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.address_books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_books_id_seq OWNER TO postgres;

--
-- Name: address_books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.address_books_id_seq OWNED BY public.address_books.id;


--
-- Name: administrators; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.administrators (
    id integer NOT NULL,
    username character varying(255),
    password character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.administrators OWNER TO postgres;

--
-- Name: administrators_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.administrators_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administrators_id_seq OWNER TO postgres;

--
-- Name: administrators_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.administrators_id_seq OWNED BY public.administrators.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    parent_id integer,
    title character varying(255),
    description character varying(255),
    image character varying(255),
    sort_order integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.countries (
    id integer NOT NULL,
    name character varying(255),
    iso_code character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.countries OWNER TO postgres;

--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.countries_id_seq OWNER TO postgres;

--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.countries_id_seq OWNED BY public.countries.id;


--
-- Name: currencies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.currencies (
    id integer NOT NULL,
    title character varying(255),
    code character varying(255),
    symbol_left character varying(255),
    symbol_right character varying(255),
    decimal_point character varying(255),
    value double precision,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.currencies OWNER TO postgres;

--
-- Name: currencies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.currencies_id_seq OWNER TO postgres;

--
-- Name: currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.currencies_id_seq OWNED BY public.currencies.id;


--
-- Name: customers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customers (
    id integer NOT NULL,
    default_address_id integer,
    gender character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    dob timestamp with time zone,
    email_address character varying(255),
    telephone character varying(255),
    fax character varying(255),
    password character varying(255),
    date_of_last_logon timestamp with time zone,
    number_of_logons integer,
    password_reset_key character varying(255),
    password_reset_date timestamp with time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.customers OWNER TO postgres;

--
-- Name: customers_baskets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customers_baskets (
    id integer NOT NULL,
    customers_id integer,
    products_id integer,
    basket_quantity integer,
    final_price double precision,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.customers_baskets OWNER TO postgres;

--
-- Name: customers_baskets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customers_baskets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customers_baskets_id_seq OWNER TO postgres;

--
-- Name: customers_baskets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customers_baskets_id_seq OWNED BY public.customers_baskets.id;


--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customers_id_seq OWNER TO postgres;

--
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customers_id_seq OWNED BY public.customers.id;


--
-- Name: manufacturers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.manufacturers (
    id integer NOT NULL,
    manufacturers_name character varying(255),
    manufacturers_image character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.manufacturers OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.manufacturers_id_seq OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.manufacturers_id_seq OWNED BY public.manufacturers.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    id integer NOT NULL,
    customers_id integer,
    customers_name character varying(255),
    customers_company character varying(255),
    customers_street_address character varying(255),
    customers_suburb character varying(255),
    customers_city character varying(255),
    customers_postcode character varying(255),
    customers_state character varying(255),
    customers_country character varying(255),
    customers_telephone character varying(255),
    customers_email_address character varying(255),
    customers_address_format_id integer,
    delivery_name character varying(255),
    delivery_company character varying(255),
    delivery_street_address character varying(255),
    delivery_suburb character varying(255),
    delivery_city character varying(255),
    delivery_postcode character varying(255),
    delivery_state character varying(255),
    delivery_country character varying(255),
    delivery_address_format_id integer,
    billing_name character varying(255),
    billing_company character varying(255),
    billing_street_address character varying(255),
    billing_suburb character varying(255),
    billing_city character varying(255),
    billing_postcode character varying(255),
    billing_state character varying(255),
    billing_country character varying(255),
    billing_address_format_id integer,
    payment_method character varying(255),
    cc_type character varying(255),
    cc_owner character varying(255),
    cc_number character varying(255),
    cc_expires timestamp with time zone,
    orders_status integer,
    currency character varying(255),
    currency_value double precision,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_id_seq OWNER TO postgres;

--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- Name: orders_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders_products (
    id integer NOT NULL,
    orders_id integer,
    products_id integer,
    products_model character varying(255),
    products_name character varying(255),
    products_price double precision,
    final_price double precision,
    products_tax double precision,
    products_quantity integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.orders_products OWNER TO postgres;

--
-- Name: orders_products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_products_id_seq OWNER TO postgres;

--
-- Name: orders_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_products_id_seq OWNED BY public.orders_products.id;


--
-- Name: orders_statuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders_statuses (
    id integer NOT NULL,
    orders_status_name character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.orders_statuses OWNER TO postgres;

--
-- Name: orders_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_statuses_id_seq OWNER TO postgres;

--
-- Name: orders_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_statuses_id_seq OWNED BY public.orders_statuses.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id integer NOT NULL,
    name character varying(255),
    description character varying(255),
    quantity integer,
    model character varying(255),
    image character varying(255),
    price double precision,
    date_available timestamp with time zone,
    weight double precision,
    status integer,
    manufacturers_id integer,
    ordered integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.products OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: products_to_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products_to_categories (
    id integer NOT NULL,
    products_id integer,
    categories_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.products_to_categories OWNER TO postgres;

--
-- Name: products_to_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_to_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_to_categories_id_seq OWNER TO postgres;

--
-- Name: products_to_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_to_categories_id_seq OWNED BY public.products_to_categories.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address_books ALTER COLUMN id SET DEFAULT nextval('public.address_books_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.administrators ALTER COLUMN id SET DEFAULT nextval('public.administrators_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.countries ALTER COLUMN id SET DEFAULT nextval('public.countries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.currencies ALTER COLUMN id SET DEFAULT nextval('public.currencies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers ALTER COLUMN id SET DEFAULT nextval('public.customers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers_baskets ALTER COLUMN id SET DEFAULT nextval('public.customers_baskets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manufacturers ALTER COLUMN id SET DEFAULT nextval('public.manufacturers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders_products ALTER COLUMN id SET DEFAULT nextval('public.orders_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders_statuses ALTER COLUMN id SET DEFAULT nextval('public.orders_statuses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products_to_categories ALTER COLUMN id SET DEFAULT nextval('public.products_to_categories_id_seq'::regclass);


--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SequelizeMeta" (name) FROM stdin;
20180720194311-create-todo.js
20180721174351-create-administrators.js
20180721180016-create-address-book.js
20180721180544-create-categories.js
20180721181429-create-countries.js
20180721183054-create-currencies.js
20180721184409-create-customers.js
20180721184849-create-customers-basket.js
20180721185137-create-manufacturers.js
20180721190355-create-orders.js
20180721191030-create-orders-products.js
20180721191147-create-orders-status.js
20180721192002-create-products.js
20180721192210-create-products-to-categories.js
\.


--
-- Data for Name: address_books; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.address_books (id, customers_id, gender, company, first_name, last_name, street_address, suburb, postcode, city, state, country_id, zone_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: address_books_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.address_books_id_seq', 1, false);


--
-- Data for Name: administrators; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.administrators (id, username, password, "createdAt", "updatedAt") FROM stdin;
2	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
3	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
4	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
5	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
6	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
7	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
8	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
9	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
10	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
11	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
12	admin	admin123	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
\.


--
-- Name: administrators_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.administrators_id_seq', 12, true);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories (id, parent_id, title, description, image, sort_order, "createdAt", "updatedAt") FROM stdin;
1	0	Fashion	Fashion	default.png	0	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
2	0	Fashion	Fashion	default.png	0	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
3	0	Trends	Trends	default.png	0	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_id_seq', 3, true);


--
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.countries (id, name, iso_code, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: countries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.countries_id_seq', 1, false);


--
-- Data for Name: currencies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.currencies (id, title, code, symbol_left, symbol_right, decimal_point, value, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: currencies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.currencies_id_seq', 1, false);


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customers (id, default_address_id, gender, first_name, last_name, dob, email_address, telephone, fax, password, date_of_last_logon, number_of_logons, password_reset_key, password_reset_date, "createdAt", "updatedAt") FROM stdin;
1	1	M	Hardev	Sharma	1986-09-27 05:30:00+05:30	hardev@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fsd1s2fs1f2sdf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
2	1	M	Ravinder	Kumar	1996-09-27 05:30:00+05:30	ravi@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fgh45fg54hfg45hfghf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
3	1	M	Hardev	Sharma	1986-09-27 05:30:00+05:30	hardev@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fsd1s2fs1f2sdf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
4	1	M	Ravinder	Kumar	1996-09-27 05:30:00+05:30	ravi@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fgh45fg54hfg45hfghf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
5	1	M	Hardev	Sharma	1986-09-27 05:30:00+05:30	hardev@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fsd1s2fs1f2sdf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
6	1	M	Ravinder	Kumar	1996-09-27 05:30:00+05:30	ravi@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fgh45fg54hfg45hfghf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
7	1	M	Hardev	Sharma	1986-09-27 05:30:00+05:30	hardev@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fsd1s2fs1f2sdf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
8	1	M	Ravinder	Kumar	1996-09-27 05:30:00+05:30	ravi@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fgh45fg54hfg45hfghf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
9	1	M	Hardev	Sharma	1986-09-27 05:30:00+05:30	hardev@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fsd1s2fs1f2sdf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
10	1	M	Ravinder	Kumar	1996-09-27 05:30:00+05:30	ravi@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fgh45fg54hfg45hfghf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
11	1	M	Hardev	Sharma	1986-09-27 05:30:00+05:30	hardev@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fsd1s2fs1f2sdf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
12	1	M	Ravinder	Kumar	1996-09-27 05:30:00+05:30	ravi@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fgh45fg54hfg45hfghf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
13	1	M	Hardev	Sharma	1986-09-27 05:30:00+05:30	hardev@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fsd1s2fs1f2sdf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
14	1	M	Ravinder	Kumar	1996-09-27 05:30:00+05:30	ravi@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fgh45fg54hfg45hfghf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
15	1	M	Hardev	Sharma	1986-09-27 05:30:00+05:30	hardev@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fsd1s2fs1f2sdf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
16	1	M	Ravinder	Kumar	1996-09-27 05:30:00+05:30	ravi@gmail.com	9876528433	1234567890	123456	2018-07-22 05:30:00+05:30	10	fgh45fg54hfg45hfghf45	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30	2018-07-22 05:30:00+05:30
\.


--
-- Data for Name: customers_baskets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customers_baskets (id, customers_id, products_id, basket_quantity, final_price, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: customers_baskets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customers_baskets_id_seq', 1, false);


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customers_id_seq', 16, true);


--
-- Data for Name: manufacturers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.manufacturers (id, manufacturers_name, manufacturers_image, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: manufacturers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.manufacturers_id_seq', 1, false);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders (id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, orders_status, currency, currency_value, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_id_seq', 1, false);


--
-- Data for Name: orders_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders_products (id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: orders_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_products_id_seq', 1, false);


--
-- Data for Name: orders_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders_statuses (id, orders_status_name, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: orders_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_statuses_id_seq', 1, false);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, name, description, quantity, model, image, price, date_available, weight, status, manufacturers_id, ordered, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 1, false);


--
-- Data for Name: products_to_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products_to_categories (id, products_id, categories_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: products_to_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_to_categories_id_seq', 1, false);


--
-- Name: SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: address_books_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address_books
    ADD CONSTRAINT address_books_pkey PRIMARY KEY (id);


--
-- Name: administrators_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.administrators
    ADD CONSTRAINT administrators_pkey PRIMARY KEY (id);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: countries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: currencies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.currencies
    ADD CONSTRAINT currencies_pkey PRIMARY KEY (id);


--
-- Name: customers_baskets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers_baskets
    ADD CONSTRAINT customers_baskets_pkey PRIMARY KEY (id);


--
-- Name: customers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: manufacturers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manufacturers
    ADD CONSTRAINT manufacturers_pkey PRIMARY KEY (id);


--
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: orders_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders_products
    ADD CONSTRAINT orders_products_pkey PRIMARY KEY (id);


--
-- Name: orders_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders_statuses
    ADD CONSTRAINT orders_statuses_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: products_to_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products_to_categories
    ADD CONSTRAINT products_to_categories_pkey PRIMARY KEY (id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

