'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      customers_id: {
        type: Sequelize.INTEGER
      },
      customers_name: {
        type: Sequelize.STRING
      },
      customers_company: {
        type: Sequelize.STRING
      },
      customers_street_address: {
        type: Sequelize.STRING
      },
      customers_suburb: {
        type: Sequelize.STRING
      },
      customers_city: {
        type: Sequelize.STRING
      },
      customers_postcode: {
        type: Sequelize.STRING
      },
      customers_state: {
        type: Sequelize.STRING
      },
      customers_country: {
        type: Sequelize.STRING
      },
      customers_telephone: {
        type: Sequelize.STRING
      },
      customers_email_address: {
        type: Sequelize.STRING
      },
      customers_address_format_id: {
        type: Sequelize.INTEGER
      },
      delivery_name: {
        type: Sequelize.STRING
      },
      delivery_company: {
        type: Sequelize.STRING
      },
      delivery_street_address: {
        type: Sequelize.STRING
      },
      delivery_suburb: {
        type: Sequelize.STRING
      },
      delivery_city: {
        type: Sequelize.STRING
      },
      delivery_postcode: {
        type: Sequelize.STRING
      },
      delivery_state: {
        type: Sequelize.STRING
      },
      delivery_country: {
        type: Sequelize.STRING
      },
      delivery_address_format_id: {
        type: Sequelize.INTEGER
      },
      billing_name: {
        type: Sequelize.STRING
      },
      billing_company: {
        type: Sequelize.STRING
      },
      billing_street_address: {
        type: Sequelize.STRING
      },
      billing_suburb: {
        type: Sequelize.STRING
      },
      billing_city: {
        type: Sequelize.STRING
      },
      billing_postcode: {
        type: Sequelize.STRING
      },
      billing_state: {
        type: Sequelize.STRING
      },
      billing_country: {
        type: Sequelize.STRING
      },
      billing_address_format_id: {
        type: Sequelize.INTEGER
      },
      payment_method: {
        type: Sequelize.STRING
      },
      cc_type: {
        type: Sequelize.STRING
      },
      cc_owner: {
        type: Sequelize.STRING
      },
      cc_number: {
        type: Sequelize.STRING
      },
      cc_expires: {
        type: Sequelize.DATE
      },
      orders_status: {
        type: Sequelize.INTEGER
      },
      currency: {
        type: Sequelize.STRING
      },
      currency_value: {
        type: Sequelize.DOUBLE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('orders');
  }
};