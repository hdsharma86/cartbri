'use strict';
module.exports = (sequelize, DataTypes) => {
  var orders_status = sequelize.define('orders_status', {
    orders_status_name: DataTypes.STRING
  }, {});
  orders_status.associate = function(models) {
    // associations can be defined here
  };
  return orders_status;
};