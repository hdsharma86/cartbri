'use strict';
module.exports = (sequelize, DataTypes) => {
  var address_book = sequelize.define('address_book', {
    customers_id: DataTypes.INTEGER,
    gender: DataTypes.STRING,
    company: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    street_address: DataTypes.STRING,
    suburb: DataTypes.STRING,
    postcode: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    country_id: DataTypes.INTEGER,
    zone_id: DataTypes.INTEGER
  }, {});
  address_book.associate = function(models) {
    // associations can be defined here
  };
  return address_book;
};