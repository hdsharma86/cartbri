'use strict';
module.exports = (sequelize, DataTypes) => {
  var products = sequelize.define('products', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    model: DataTypes.STRING,
    image: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    date_available: DataTypes.DATE,
    weight: DataTypes.FLOAT,
    status: DataTypes.INTEGER,
    manufacturers_id: DataTypes.INTEGER,
    ordered: DataTypes.INTEGER
  }, {});
  products.associate = function(models) {
    // associations can be defined here
  };
  return products;
};