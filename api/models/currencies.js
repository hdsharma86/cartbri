'use strict';
module.exports = (sequelize, DataTypes) => {
  var currencies = sequelize.define('currencies', {
    title: DataTypes.STRING,
    code: DataTypes.STRING,
    symbol_left: DataTypes.STRING,
    symbol_right: DataTypes.STRING,
    decimal_point: DataTypes.STRING,
    value: DataTypes.FLOAT
  }, {});
  currencies.associate = function(models) {
    // associations can be defined here
  };
  return currencies;
};