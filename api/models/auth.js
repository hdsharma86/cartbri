'use strict';

var bcrypt = require('bcrypt-nodejs');
module.exports = (sequelize, DataTypes) => {

    var userSchema = sequelize.define('customers', {
        name: DataTypes.STRING,
        description: DataTypes.TEXT
    }, {
        schema: 'user',
        local: {
            email: DataTypes.STRING,
            password: DataTypes.STRING
        },
        facebook: {
            id: DataTypes.STRING,
            token: DataTypes.STRING,
            name: DataTypes.STRING,
            email: DataTypes.STRING
        },
        twitter: {
            id: DataTypes.STRING,
            token: DataTypes.STRING,
            displayName: DataTypes.STRING,
            username: DataTypes.STRING
        },
        google: {
            id: DataTypes.STRING,
            token: DataTypes.STRING,
            email: DataTypes.STRING,
            name: DataTypes.STRING
        }
    });

    userSchema.generateHash = function (password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    };

// checking if password is valid
    userSchema.validPassword = function (password) {
        return bcrypt.compareSync(password, this.local.password);
    };

    userSchema.associate = function (models) {
        // associations can be defined here
    };
    return userSchema;
};
