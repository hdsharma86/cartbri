'use strict';
module.exports = (sequelize, DataTypes) => {
  var countries = sequelize.define('countries', {
    name: DataTypes.STRING,
    iso_code: DataTypes.STRING
  }, {});
  countries.associate = function(models) {
    // associations can be defined here
  };
  return countries;
};