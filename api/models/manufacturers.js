'use strict';
module.exports = (sequelize, DataTypes) => {
  var manufacturers = sequelize.define('manufacturers', {
    manufacturers_name: DataTypes.STRING,
    manufacturers_image: DataTypes.STRING
  }, {});
  manufacturers.associate = function(models) {
    // associations can be defined here
  };
  return manufacturers;
};