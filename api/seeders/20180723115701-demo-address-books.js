'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('address_books', [{
      customers_id: 1,
      gender: "M", 
      company: "ABC",
      first_name: "Hardev",
      last_name: "Sharma",
      street_address: "#109 Chandigarh",
      suburb: "Chandigarh",
      postcode: "123456",
      city: "Chandigarh",
      state: "Punjab",
      country_id: 1,
      zone_id: 1,
      createdAt: "2018-07-22",
      updatedAt: "2018-07-22"
    },
    {
      customers_id: 2,
      gender: "M", 
      company: "ABC",
      first_name: "Naveen",
      last_name: "Kumar",
      street_address: "#109 Chandigarh",
      suburb: "Chandigarh",
      postcode: "123456",
      city: "Chandigarh",
      state: "Punjab",
      country_id: 1,
      zone_id: 1,
      createdAt: "2018-07-22",
      updatedAt: "2018-07-22"
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('address_books', null, {});
  }
};
