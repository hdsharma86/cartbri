'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('orders', [{
      customers_id: '1',
      customers_name: "Hardev",
      customers_company: "ABC",
      customers_street_address: "#109 Mohali",
      customers_suburb: "XYZ",
      customers_city: "Mohali",
      customers_postcode: "123456",
      customers_state: "Punjab",
      customers_country: "India",
      customers_telephone: "1234567890",
      customers_email_address: "hardev@cartbri.com",
      delivery_name: "Hardev",
      delivery_company: "ABC",
      delivery_street_address: "#109, Mohali",
      delivery_suburb: "Mohali",
      delivery_city: "Mohali",
      delivery_postcode: "123456",
      delivery_state: "Punjab",
      delivery_country: "India",
      billing_name: "Hardev",
      billing_company: "ABC",
      billing_street_address: "#109, Mohali",
      billing_suburb: "Mohali",
      billing_city: "Mohali",
      billing_postcode: "123456",
      billing_state: "Punjab",
      billing_country: "India",
      payment_method: "COD",
      cc_type: "Visa",
      cc_owner: "Hardev",
      cc_number: "12345678912345678",
      cc_expires: "2020-08-22",
      orders_status: 1,
      currency: "INR",
      currency_value: "123.00",
      createdAt: "2018-07-22",
      updatedAt: "2018-07-22"
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('orders', null, {});
  }
};
