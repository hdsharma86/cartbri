'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('categories', [{
      parent_id: 0,
      title: "Fashion",
      description: "Fashion",
      image: "default.png",
      sort_order: "0",
      createdAt: "2018-07-22",
      updatedAt: "2018-07-22"
    },
    {
      parent_id: 0,
      title: "Trends",
      description: "Trends",
      image: "default.png",
      sort_order: "0",
      createdAt: "2018-07-22",
      updatedAt: "2018-07-22"
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('categories', null, {});
  }
};
