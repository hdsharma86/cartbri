'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('administrators', [{
      username: 'admin',
      password: 'admin123',
      createdAt: '2018-07-22',
      updatedAt: '2018-07-22'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('administrators', null, {});
  }
};
