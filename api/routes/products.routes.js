'use strict';

/**
 * Product routes...
 * @param {*} app 
 */
module.exports = function(app){
    var products = require('../controllers/products.controller');

    /**
     * @swagger
     * /api/products:
     *   post:
     *     tags:
     *       - Products
     *     description: Creates a new product
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: tarun
     *         description: product object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/product'
     *     responses:
     *       200:
     *         description: Successfully created
     */
    app.post('/api/products', products.create);

    /**
     * @swagger
     * /api/products:
     *   get:
     *     tags:
     *       - Products
     *     description: Returns all products
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Return all products.
     *         schema:
     *           $ref: '#/definitions/products'
     */
    app.get('/api/products', products.findAll);

    /**
     * @swagger
     * /api/products/{productId}:
     *   get:
     *     tags:
     *       - Products
     *     description: Returns a single product based on the supplied identity.
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Returns a single product based on the supplied identity.
     *         schema:
     *           $ref: '#/definitions/products'
     */
    app.get('/api/products/:productId', products.findById);

    /**
     * @swagger
     * /api/products/{productId}:
     *   put:
     *     tags: 
     *      - Products
     *     description: Updates a single product
     *     produces: application/json
     *     parameters:
     *       firstname: Hardev
     *       in: body
     *       description: Fields for the product resource
     *       schema:
     *         type: array
     *         $ref: '#/definitions/product'
     *     responses:
     *       200:
     *         description: Successfully updated
     */
    app.put('/api/products/:productId', products.update);

    /**
     * @swagger
     * /api/product/{productId}:
     *   delete:
     *     tags:
     *       - Products
     *     description: Deletes a single product
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: product's id
     *         in: path
     *         required: true
     *         type: integer
     *     responses:
     *       200:
     *         description: Successfully deleted
     */
    app.delete('/api/products/:productId', products.delete);
};