'use strict';

const routes = [
    require('./customer.routes'),
    require('./products.routes'),
    require('./categories.routes'),
    require('./orders.routes')
];

module.exports = function router(app) {
    return routes.forEach((route) => {
      route(app);
    });
};

