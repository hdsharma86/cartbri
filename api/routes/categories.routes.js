'use strict';

module.exports = function(app){
    var categories = require('../controllers/categories.controller');

    /**
     * @swagger
     * /api/categories:
     *   post:
     *     tags:
     *       - Categories
     *     description: Creates a new category
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: tarun
     *         description: category object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/category'
     *     responses:
     *       200:
     *         description: Successfully created
     */
    app.post('/api/categories', categories.createCategory);

    /**
     * @swagger
     * /api/categories:
     *   get:
     *     tags:
     *       - Categories
     *     description: Returns all categories
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Return all categories.
     *         schema:
     *           $ref: '#/definitions/categories'
     */
    app.get('/api/categories', categories.findAll);

    /**
     * @swagger
     * /api/categories/{categoryId}:
     *   get:
     *     tags:
     *       - Categories
     *     description: Returns a single category based on the supplied identity.
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Returns a single category based on the supplied identity.
     *         schema:
     *           $ref: '#/definitions/categories'
     */
    app.get('/api/categories/:categoryId', categories.findById);

    /**
     * @swagger
     * /api/categories/{categoryId}:
     *   put:
     *     tags: 
     *      - Categories
     *     description: Updates a single category
     *     produces: application/json
     *     parameters:
     *       firstname: Hardev
     *       in: body
     *       description: Fields for the category resource
     *       schema:
     *         type: array
     *         $ref: '#/definitions/category'
     *     responses:
     *       200:
     *         description: Successfully updated
     */
    app.put('/api/categories/:categoryId', categories.update);

    /**
     * @swagger
     * /api/category/{categoryId}:
     *   delete:
     *     tags:
     *       - Categories
     *     description: Deletes a single category
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: category's id
     *         in: path
     *         required: true
     *         type: integer
     *     responses:
     *       200:
     *         description: Successfully deleted
     */
    app.delete('/api/categories/:categoryId', categories.delete);
};