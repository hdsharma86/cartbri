'use strict';

module.exports = function (app, passport) {
    var auth = require('../controllers/auth.controller');

    /**
     * @swagger
     * /api/login:
     *   post:
     *     tags:
     *       - User
     *     description: Login user
     *     produces:
     *       - application/json
     *     parameters:
     *       - email: string
     *         password: string
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/user'
     *     responses:
     *       200:
     *         description: Successfully created
     */
    app.post('/api/login', passport.authenticate('local-login', {
        successRedirect: '/profile', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));
    // process the login form
    // app.post('/login', do all our passport stuff here);

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/api/signup', passport.authenticate('local-signup', {
        successRedirect: '/profile', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // process the signup form
    // app.post('/signup', do all our passport stuff here);

    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/api/profile', isLoggedIn, auth.userProfile);

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/api/logout', auth.userLogout);
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}