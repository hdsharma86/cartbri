'use strict';

module.exports = function(app){
    var orders = require('../controllers/orders.controller');

    /**
     * @swagger
     * /api/orders:
     *   post:
     *     tags:
     *       - Orders
     *     description: Creates a new Order
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: tarun
     *         description: Order object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/Order'
     *     responses:
     *       200:
     *         description: Successfully created
     */
    app.post('/api/orders', orders.createOrder);

    /**
     * @swagger
     * /api/orders:
     *   get:
     *     tags:
     *       - Orders
     *     description: Returns all orders
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Return all orders.
     *         schema:
     *           $ref: '#/definitions/orders'
     */
    app.get('/api/orders', orders.findAll);

    /**
     * @swagger
     * /api/orders/{OrderId}:
     *   get:
     *     tags:
     *       - Orders
     *     description: Returns a single Order based on the supplied identity.
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Returns a single Order based on the supplied identity.
     *         schema:
     *           $ref: '#/definitions/orders'
     */
    app.get('/api/orders/:OrderId', orders.findById);

    /**
     * @swagger
     * /api/orders/{OrderId}:
     *   put:
     *     tags: 
     *      - Orders
     *     description: Updates a single Order
     *     produces: application/json
     *     parameters:
     *       firstname: Hardev
     *       in: body
     *       description: Fields for the Order resource
     *       schema:
     *         type: array
     *         $ref: '#/definitions/Order'
     *     responses:
     *       200:
     *         description: Successfully updated
     */
    app.put('/api/orders/:OrderId', orders.update);

    /**
     * @swagger
     * /api/Order/{OrderId}:
     *   delete:
     *     tags:
     *       - Orders
     *     description: Deletes a single Order
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Order's id
     *         in: path
     *         required: true
     *         type: integer
     *     responses:
     *       200:
     *         description: Successfully deleted
     */
    app.delete('/api/orders/:OrderId', orders.delete);
};