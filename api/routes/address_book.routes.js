'use strict';

module.exports = function(app){
    var addressBook = require('../controllers/address_book.controller');

    /**
     * @swagger
     * /api/address-books:
     *   post:
     *     tags:
     *       - Address Books
     *     description: Creates a new category
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: tarun
     *         description: category object
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/category'
     *     responses:
     *       200:
     *         description: Successfully created
     */
    app.post('/api/address-books', addressBook.createCategory);

    /**
     * @swagger
     * /api/address-books:
     *   get:
     *     tags:
     *       - Address Books
     *     description: Returns all address books
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Return all addressBook.
     *         schema:
     *           $ref: '#/definitions/address-books'
     */
    app.get('/api/address-books', addressBook.findAll);

    /**
     * @swagger
     * /api/address-books/{addressBookId}:
     *   get:
     *     tags:
     *       - Address Books
     *     description: Returns a single category based on the supplied identity.
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Returns a single category based on the supplied identity.
     *         schema:
     *           $ref: '#/definitions/address-books'
     */
    app.get('/api/address-books/:addressBookId', addressBook.findById);

    /**
     * @swagger
     * /api/address-books/{addressBookId}:
     *   put:
     *     tags: 
     *      - Address Books
     *     description: Updates a single category
     *     produces: application/json
     *     parameters:
     *       firstname: Hardev
     *       in: body
     *       description: Fields for the category resource
     *       schema:
     *         type: array
     *         $ref: '#/definitions/category'
     *     responses:
     *       200:
     *         description: Successfully updated
     */
    app.put('/api/address-books/:addressBookId', addressBook.update);

    /**
     * @swagger
     * /api/address-books/{addressBookId}:
     *   delete:
     *     tags:
     *       - Address Books
     *     description: Deletes a single category
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: category's id
     *         in: path
     *         required: true
     *         type: integer
     *     responses:
     *       200:
     *         description: Successfully deleted
     */
    app.delete('/api/address-books/:addressBookId', addressBook.delete);
};