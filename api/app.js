const express = require('express')
const app = express()
var swaggerJSDoc = require('swagger-jsdoc');

var passport = require('passport');
var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var session      = require('express-session');


// swagger definition
var swaggerDefinition = {
  info: {
    title: 'API Documentation',
    version: '1.0.0',
    description: 'Demonstrating how to describe a RESTful API with Swagger',
  },
  host: 'localhost:8000',
  basePath: '/',
};

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./routes/*.routes.js'],// pass all in array 

  };

require('./config/passport')(passport); // pass passport for configuration

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);
// serve swagger 
app.get('/swagger.json', function(req, res) {   
  res.setHeader('Content-Type', 'application/json');   
  res.send(swaggerSpec); 
});
const bodyParser = require('body-parser');
const router = require('./routes/index');

var swaggerUi = require('swagger-ui-express');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
//require('../routes/auth.routes')(app, passport); // load our routes and pass in our app and fully configured passport


const port = process.env.port || 8000;

app.get('/', function (req, res) {
  res.send('Hello World!')
})

router(app);
module.exports = app;