var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.json')[env];
const pgPromise = require('pg-promise');
//const connStr = 'postgresql://postgres@127.0.0.1:5432/cartbri'; // add your psql details
const connectionString = 'postgresql://'+config.username+'@'+config.host+':'+config.port+'/'+config.database;
const pgp = pgPromise({}); // empty pgPromise instance
const psql = pgp(connectionString); // get connection to your db instance
exports.psql = psql;