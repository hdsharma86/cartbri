'use strict';

const models = require('../models');
const addressBook = models.address_book;

/**
 * Create a new addressBook.
 * @param {*} req 
 * @param {*} res 
 */
exports.createAddressBook = (req,res) => {
    var data = req.body;
    var date = new Date();
    var currentDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    
    addressBook.create({
        customers_id: data.customers_id,
        gender: data.gender, 
        company: data.company,
        first_name: data.first_name,
        last_name: data.last_name,
        street_address: data.street_address,
        suburb: data.suburb,
        postcode: data.postcode,
        city: data.city,
        state: data.state,
        country_id: data.country_id,
        zone_id: data.zone_id,
        createdAt: currentDate,
        updatedAt: currentDate
    }).then(addressBook => {
        res.json(addressBook);
    });
};

/**
 * Function to fetch all categories...
 * @param {*} req 
 * @param {*} res 
 */
exports.findAll = (req, res) => {
    addressBook.findAll().then(categories => {
        res.json(categories);
    })
};

/**
 * Function to fetch addressBook based on the supplied identity...
 * @param {*} req 
 * @param {*} res 
 */
exports.findById = (req, res) => {
    var addressBookId = req.params.addressBookId;
    addressBook.findById(addressBookId).then(addressBook => {
        res.json(addressBook);
    });
};

/**
 * Function to update addressBook...
 * @param {*} req 
 * @param {*} res 
 */
exports.update = (req, res) => {
    var data = req.body;
    var addressBookId = req.params.addressBookId;
    addressBook.update({ 
            title: data.gender,
            description: data.description, 
            image: data.image
        }, 
        { 
            where: {id: addressBookId} 
    }).then(() => {
        res.status(200).json("Updated successfully a addressBook with id = " + addressBookId);
    });
};

exports.delete = (req,res) => {
    var addressBookId = req.params.addressBookId;
    addressBook.destroy({
        where: {id: addressBookId}
    }).then(() => {
        res.status(200).json("addressBook deleted successfully...");
    });
};