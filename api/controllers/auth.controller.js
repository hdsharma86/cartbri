'use strict';

const models = require('../models');
const Auth = models.auth;

/**
 * Handle user login.
 * @param {*} req 
 * @param {*} res 
 */
exports.userLogin = (req, res) => {
    // render the page and pass in any flash data if it exists
    res.render('login.ejs', {message: req.flash('loginMessage')});
};

exports.userSignup = (req, res) => {
    res.render('signup.ejs', {message: req.flash('signupMessage')});
};

exports.userProfile = (req, res) => {
    res.render('profile.ejs', {
        user: req.user // get the user out of session and pass to template
    });
};

exports.userLogout = (req, res) => {
    req.logout();
    res.redirect('/');
};

