'use strict';

const models = require('../models');
const Category = models.categories;

/**
 * Create a new Category.
 * @param {*} req 
 * @param {*} res 
 */
exports.createCategory = (req,res) => {
    var data = req.body;
    var date = new Date();
    var currentDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    
    Category.create({
        parent_id: 0,
        title: data.firstname, 
        description: data.lastname,
        image: data.dob,
        sort_order: data.email,
        createdAt: currentDate,
        updatedAt: currentDate
    }).then(Category => {
        res.json(Category);
    });
};

/**
 * Function to fetch all categories...
 * @param {*} req 
 * @param {*} res 
 */
exports.findAll = (req, res) => {
    Category.findAll().then(categories => {
        res.json(categories);
    })
};

/**
 * Function to fetch Category based on the supplied identity...
 * @param {*} req 
 * @param {*} res 
 */
exports.findById = (req, res) => {
    var CategoryId = req.params.CategoryId;
    Category.findById(CategoryId).then(Category => {
        res.json(Category);
    });
};

/**
 * Function to update Category...
 * @param {*} req 
 * @param {*} res 
 */
exports.update = (req, res) => {
    var data = req.body;
    var CategoryId = req.params.CategoryId;
    Category.update({ 
            title: data.gender,
            description: data.description, 
            image: data.image
        }, 
        { 
            where: {id: CategoryId} 
    }).then(() => {
        res.status(200).json("Updated successfully a Category with id = " + CategoryId);
    });
};

exports.delete = (req,res) => {
    var CategoryId = req.params.CategoryId;
    Category.destroy({
        where: {id: CategoryId}
    }).then(() => {
        res.status(200).json("Category deleted successfully...");
    });
};