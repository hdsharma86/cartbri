'use strict';

const models = require('../models');
const Order = models.orders;

/**
 * Create a new Order.
 * @param {*} req 
 * @param {*} res 
 */
exports.createOrder = (req,res) => {
    var data = req.body;
    var date = new Date();
    var currentDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    
    Order.create({
        customers_id: data.customer_id,
        customers_name: data.customers_name,
        customers_company: data.customers_company,
        customers_street_address: data.customers_street_address,
        customers_suburb: data.customers_suburb,
        customers_city: cata.customers_city,
        customers_postcode: data.customers_postcode,
        customers_state: data.customers_state,
        customers_country: data.customers_country,
        customers_telephone: data.customers_telephone,
        customers_email_address: data.customers_email_address,
        delivery_name: data.delivery_name,
        delivery_company: data.delivery_company,
        delivery_street_address: data.delivery_street_address,
        delivery_suburb: data.delivery_suburb,
        delivery_city: data.delivery_city,
        delivery_postcode: data.delivery_postcode,
        delivery_state: data.delivery_state,
        delivery_country: data.delivery_country,
        billing_name: date.billing_name,
        billing_company: data.billing_company,
        billing_street_address: data.billing_street_address,
        billing_suburb: data.billing_suburb,
        billing_city: data.billing_city,
        billing_postcode: data.billing_postcode,
        billing_state: data.billing_state,
        billing_country: data.billing_country,
        payment_method: data.payment_method,
        cc_type: data.cc_type,
        cc_owner: data.cc_owner,
        cc_number: data.cc_number,
        cc_expires: data.cc_expires,
        orders_status: data.orders_status,
        currency: data.currency,
        currency_value: data.currency_value
    }).then(Order => {
        res.json(Order);
    });
};

/**
 * Function to fetch all orders...
 * @param {*} req 
 * @param {*} res 
 */
exports.findAll = (req, res) => {
    Order.findAll().then(orders => {
        res.json(orders);
    })
};

/**
 * Function to fetch Order based on the supplied identity...
 * @param {*} req 
 * @param {*} res 
 */
exports.findById = (req, res) => {
    var OrderId = req.params.OrderId;
    Order.findById(OrderId).then(Order => {
        res.json(Order);
    });
};

/**
 * Function to update Order...
 * @param {*} req 
 * @param {*} res 
 */
exports.update = (req, res) => {
    var data = req.body;
    var OrderId = req.params.OrderId;
    Order.update({
        // Update code here...
    }, { 
        where: {id: OrderId} 
    }).then(() => {
        res.status(200).json("Updated successfully a Order with id = " + OrderId);
    });
};

exports.delete = (req,res) => {
    var OrderId = req.params.OrderId;
    Order.destroy({
        where: {id: OrderId}
    }).then(() => {
        res.status(200).json("Order deleted successfully...");
    });
};