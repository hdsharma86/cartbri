'use strict';

const models = require('../models');
const Customer = models.customers;

/**
 * Create a new customer.
 * @param {*} req 
 * @param {*} res 
 */
exports.createCustomer = (req,res) => {
    var data = req.body;
    var date = new Date();
    var currentDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    
    Customer.create({
        customers_gender: data.gender,
        customers_firstname: data.firstname, 
        customers_lastname: data.lastname,
        customers_dob: data.dob,
        customers_email_address: data.email,
        customers_default_address_id: 1,
        customers_telephone: data.telephone,
        customers_fax: data.fax,
        customers_password: data.password
    }).then(customer => {
        res.json(customer);
    });
};

/**
 * Function to fetch all customers...
 * @param {*} req 
 * @param {*} res 
 */
exports.findAll = (req, res) => {
    Customer.findAll().then(customers => {
        res.json(customers);
    })
};

/**
 * Function to fetch customer based on the supplied identity...
 * @param {*} req 
 * @param {*} res 
 */
exports.findById = (req, res) => {
    var customerId = req.params.customerId;
    Customer.findById(customerId).then(customer => {
        res.json(customer);
    });
};

/**
 * Function to update customer...
 * @param {*} req 
 * @param {*} res 
 */
exports.update = (req, res) => {
    var data = req.body;
    var customerId = req.params.customerId;
    Customer.update({ 
            customers_gender: data.gender,
            customers_firstname: data.firstname, 
            customers_lastname: data.lastname
        }, 
        { 
            where: {id: customerId} 
    }).then(() => {
        res.status(200).json("Updated successfully a customer with id = " + customerId);
    });
};

exports.delete = (req,res) => {
    var customerId = req.params.customerId;
    Customer.destroy({
        where: {id: customerId}
    }).then(() => {
        res.status(200).json("Customer deleted successfully...");
    });
};