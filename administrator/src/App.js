import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="card card-login mx-auto mt-5">
          <div className="card-header">Login</div>
          <div className="card-body">
            <form>
              <div className="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input className="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email" />
              </div>
              <div className="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input className="form-control" id="exampleInputPassword1" type="password" placeholder="Password" />
              </div>
              <a className="btn btn-primary btn-block" href="index.html">Login</a>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
